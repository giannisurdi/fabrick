# README #
Controller Rest che permette di gestire le seguenti operazioni sul conto

* Lettura saldo
* Lista di transazioni
* Bonifico

L’applicativo è sviluppato utilizzando le API esposte da Fabrick https://developers.fabrick.com/hc/it

* Versione: 0.0.1-SNAPSHOT

# Tecnologie richieste: #
* JDK 1.8
* Maven 3
* IDE Eclipse
* MySQL DB

Per configurare il DB è necessario modificare le relative properties contenute nel file application.properties. Lo script di creazione del DB e delle tabelle è contenuto nella cartella sql.

Per buildare il progetto lanciare dalla cartella root del workspace il seguente comando: mvn clean install.

Per mandare in esecuzione l'applicativo, eseguire il metodo main contenuto nel file it.fabrick.Main.java dal proprio IDE.

Una volta mandato in esecuzione l'applicativo, caricare la seguente pagina nel proprio browser http://localhost:8080/Fabrick/swagger-ui.html
