package it.fabrick.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import it.fabrick.exception.InputRequiredException;
import it.fabrick.message.FabrickErrorMessage;
import it.fabrick.service.FabrickService;

@RestController
public class FabrickController {
	
	private static final Logger logger = LoggerFactory.getLogger(FabrickController.class);
	
	@Autowired
	private FabrickService fabrickService;
	
	@GetMapping("/letturaSaldo")
	@ResponseBody
	public ResponseEntity<Object> getLetturaSaldo(Long accountId) throws Exception {
		
		logger.debug("Request getLetturaSaldo of accountId: {}", accountId);
		
		try {
			return new ResponseEntity<>(fabrickService.letturaSaldo(accountId),HttpStatus.OK);
		}catch(InputRequiredException e) {
			logger.warn("Warning in getLetturaSaldo request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
			
		}catch (HttpClientErrorException | HttpServerErrorException e) {
			logger.error("Error in getLetturaSaldo request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), e.getStatusCode());
		}catch(Exception e) {
			logger.error("Error in getLetturaSaldo request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping("/bonifico")
	@ResponseBody
	public ResponseEntity<Object> bonifico(Long accountId, String receiverName, String accountCode, 
			String description, String currency, String amount,String executionDate) throws Exception {
		
		logger.debug("Request bonifico of accountId: {}, receiverName: {}, accountCode: {}, description: {}, currency: {}, amount: {}, executionDate: {}"
				, accountId, receiverName, accountCode, description, currency, amount, executionDate);
		
		try {
			return new ResponseEntity<>(fabrickService.bonifico(accountId, receiverName, accountCode, description, currency, amount, executionDate),HttpStatus.OK);
		}catch(InputRequiredException e) {
			logger.warn("Warning on bonifico request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}catch (HttpClientErrorException | HttpServerErrorException e) {
			logger.error("Error on bonifico request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), e.getStatusCode());
		}catch(Exception e) {
			logger.error("Error on bonifico request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping("/letturaTransazioni")
	@ResponseBody
	public ResponseEntity<Object> letturaTransazioni(Long accountId, String fromAccountingDate, String toAccountingDate) throws Exception {
		
		logger.debug("Request letturaTransazioni of accountId: {}, fromAccountingDate: {}, toAccountingDate: {}"
				, accountId, fromAccountingDate, toAccountingDate);
		
		try {
			return new ResponseEntity<>(fabrickService.letturaTransazioni(accountId, fromAccountingDate, toAccountingDate),HttpStatus.OK);
		}catch(InputRequiredException e) {
			logger.warn("Warning in letturaTransazioni request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
			
		}catch (HttpClientErrorException | HttpServerErrorException e) {
			logger.error("Error in letturaTransazioni request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), e.getStatusCode());
		}catch(Exception e) {
			logger.error("Error in letturaTransazioni request: {}", e.getMessage());
			return new ResponseEntity<Object>(
					new FabrickErrorMessage(e).getErrorMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
