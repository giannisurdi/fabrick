package it.fabrick.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class TransazioniPayload extends Payload implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 383229720362778910L;
	private List<Transazione> list;

}
