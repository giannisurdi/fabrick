package it.fabrick.model;

import lombok.Data;

@Data
public class BonificoRequest {
	private Creditor creditor;
	private String description;
	private Integer amount;
	private String currency;
	private String executionDate;
	
	public BonificoRequest(String receiverName, String accountCode, String description, String currency, Integer amount,
			String executionDate) {
		creditor = new Creditor(receiverName, accountCode);
		this.description = description;
		this.currency = currency;
		this.amount = amount;
		this.executionDate = executionDate;
	}
}
