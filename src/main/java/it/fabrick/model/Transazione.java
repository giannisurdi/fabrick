package it.fabrick.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="transazioni")
public class Transazione implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4953111211666600672L;
	
	@Id
	private Long transactionId;
	private String operationId;
	private String accountingDate;
	private String valueDate;
	
	@Transient
	private Type type;
	
	@JsonIgnore
	private String enumerationType;
	@JsonIgnore
	private String valueType;
	
	private Long amount;
	private String currency;
	private String description;
	
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	public String getOperationId() {
		return operationId;
	}
	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}
	public String getAccountingDate() {
		return accountingDate;
	}
	public void setAccountingDate(String accountingDate) {
		this.accountingDate = accountingDate;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		setEnumerationType(type.getEnumeration());
		setValueType(type.getValue());
		this.type = type;
	}
	public String getEnumerationType() {
		return enumerationType;
	}
	public void setEnumerationType(String enumerationType) {
		this.enumerationType = enumerationType;
	}
	public String getValueType() {
		return valueType;
	}
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
