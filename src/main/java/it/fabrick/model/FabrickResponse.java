package it.fabrick.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class FabrickResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5352758130617743162L;
	private String status;
	private List<Error> error;
	private Payload payload;
}
