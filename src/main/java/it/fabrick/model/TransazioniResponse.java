package it.fabrick.model;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class TransazioniResponse extends FabrickResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8270784235050485793L;
	private TransazioniPayload payload;
}
