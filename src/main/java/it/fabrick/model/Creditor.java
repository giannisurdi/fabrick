package it.fabrick.model;

import lombok.Data;

@Data
public class Creditor {
	private String name;
	private Account account;
	
	public Creditor(String name, String accountCode) {
		this.name = name;
		account = new Account(accountCode);
	}
}
