package it.fabrick.model;

import lombok.Data;

@Data
public class Account {
	private String accountCode;
	
	public Account(String accountCode) {
		this.accountCode = accountCode;
	}
}
