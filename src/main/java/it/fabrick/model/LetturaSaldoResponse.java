package it.fabrick.model;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class LetturaSaldoResponse extends FabrickResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 213031003951859277L;
	private LetturaSaldoPayload payload;
}
