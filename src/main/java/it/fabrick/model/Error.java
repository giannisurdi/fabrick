package it.fabrick.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Error implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4150285212985711449L;
	private String code;
	private String description;
	private String params;

}
