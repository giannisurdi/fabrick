package it.fabrick.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Type implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9195472986980259183L;
	private String enumeration;
	private String value;
}
