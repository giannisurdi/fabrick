package it.fabrick.repository;

import org.springframework.data.repository.CrudRepository;

import it.fabrick.model.Transazione;

//This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//CRUD refers Create, Read, Update, Delete
public interface TransazioniRepository extends CrudRepository<Transazione, Long> {

}
