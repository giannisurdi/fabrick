package it.fabrick.exception;

public class InputRequiredException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3744826453300897501L;
	
	public InputRequiredException(String message) {
		super(message);
	}

}
