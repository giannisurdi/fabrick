package it.fabrick.message;

public class FabrickErrorMessage {
	
	private String errorMessage;
	
	public FabrickErrorMessage(Exception e) {
		errorMessage = e.getMessage();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
