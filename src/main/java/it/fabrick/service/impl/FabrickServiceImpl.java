package it.fabrick.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fabrick.exception.InputRequiredException;
import it.fabrick.model.BonificoRequest;
import it.fabrick.model.LetturaSaldoResponse;
import it.fabrick.model.Transazione;
import it.fabrick.model.FabrickResponse;
import it.fabrick.model.TransazioniResponse;
import it.fabrick.repository.TransazioniRepository;
import it.fabrick.service.FabrickService;

@Service
public class FabrickServiceImpl implements FabrickService{
	
	@Value( "${baseurl}" )
	private String baseUrl;
	
	@Value( "${saldo-path}" )
	private String saldoPath;
	
	@Value( "${bonifico-path}" )
	private String bonificoPath;
	
	@Value( "${lettura-transazioni-path}" )
	private String letturaTransazioniPath;
	
	@Value( "${auth-schema}" )
	private String authSchema;
	
	@Value( "${api-key}" )
	private String apiKey;
	
	@Value( "${content-type}" )
	private String contentType;
	
	@Value( "${key-id}" )
	private String keyId;
	
	@Autowired
	TransazioniRepository transazioniRepository;

	@Override
	public FabrickResponse letturaSaldo(Long accountId) throws Exception{
		RestTemplate restTemplate = new RestTemplate();
		
		if(accountId!=null) {
			try {
				//setting headers request
				HttpHeaders headers = new HttpHeaders();
				headers.set("Auth-Schema", authSchema);
				headers.set("Api-Key", apiKey);
				headers.set("Content-Type", contentType);
				//headers.set("Api-Key", keyId); //for internal use
				
				HttpEntity<String> entity = new HttpEntity<>("", headers);
				
				String path = saldoPath.replace("{accountId}", accountId.toString());
				
				return restTemplate.exchange(baseUrl + path, HttpMethod.GET, entity, LetturaSaldoResponse.class).getBody();
			}catch (HttpClientErrorException | HttpServerErrorException e) {
				throw e;
			}catch (Exception e) {
				throw e;
			}
		}else {
			throw new InputRequiredException("Parametri in ingresso obbligatori non popolati correttamente");
		}
		
	}

	@Override
	public Object bonifico(Long accountId, String receiverName, String accountCode, String description, String currency, String amount,
			String executionDate) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		
		if(accountId!=null && StringUtils.isNotBlank(receiverName) 
				&& StringUtils.isNotBlank(description) && StringUtils.isNotBlank(accountCode)
				&& StringUtils.isNotBlank(amount) && StringUtils.isNotBlank(currency)) {
			try {
				//setting headers request
				HttpHeaders headers = new HttpHeaders();
				headers.set("Auth-Schema", authSchema);
				headers.set("Api-Key", apiKey);
				headers.set("Content-Type", contentType);
				//headers.set("Api-Key", keyId); //for internal use
				
				//build body request
				BonificoRequest bonificoRequest= new BonificoRequest(receiverName, accountCode, description, currency, Integer.parseInt(amount), executionDate);
				
				//converting bonificoRequest into JSON string
				ObjectMapper Obj = new ObjectMapper();  
				String bodyJSON = "";
	            try {  
	                bodyJSON = Obj.writeValueAsString(bonificoRequest);
	            }catch (Exception e) {
					throw e;
				}
				
				HttpEntity<String> entity = new HttpEntity<>(bodyJSON, headers);
				
				String path  = bonificoPath.replace("{accountId}", accountId.toString());
				
				return restTemplate.exchange(baseUrl + path, HttpMethod.POST, entity, Object.class).getBody();
				
			}catch (HttpClientErrorException | HttpServerErrorException e) {
				throw e;
			}catch (Exception e) {
				throw e;
			}
		}else {
			throw new InputRequiredException("Parametri in ingresso obbligatori non popolati correttamente");
		}
		
	}

	@Override
	public FabrickResponse letturaTransazioni(Long accountId, String fromAccountingDate, String toAccountingDate)
			throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		
		if(accountId!=null && StringUtils.isNotBlank(fromAccountingDate) && StringUtils.isNotBlank(toAccountingDate)) {
			try {
				//setting headers request
				HttpHeaders headers = new HttpHeaders();
				headers.set("Auth-Schema", authSchema);
				headers.set("Api-Key", apiKey);
				headers.set("Content-Type", contentType);
				//headers.set("Api-Key", keyId); //for internal use
				
				HttpEntity<String> entity = new HttpEntity<>("", headers);
				
				String path  = letturaTransazioniPath.replace("{accountId}", accountId.toString());
				
				String url = baseUrl + path + "?fromAccountingDate=" + fromAccountingDate + "&toAccountingDate=" + toAccountingDate; 
				
				TransazioniResponse response = restTemplate.exchange(url, HttpMethod.GET, entity, TransazioniResponse.class).getBody();
				
				//save all transazioni at DB
				if(response != null && response.getPayload() != null && response.getPayload().getList() != null) {
					for(Transazione transazione : response.getPayload().getList()) {
						transazioniRepository.save(transazione);
					}
				}
				
				return response;
				
			}catch (HttpClientErrorException | HttpServerErrorException e) {
				throw e;
			}catch (Exception e) {
				throw e;
			}
		}else {
			throw new InputRequiredException("Parametri in ingresso obbligatori non popolati correttamente");
		}
	}

}
