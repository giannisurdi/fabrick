package it.fabrick.service;

import it.fabrick.model.FabrickResponse;

public interface FabrickService {
	
	FabrickResponse letturaSaldo(Long accountId) throws Exception;
	
	Object bonifico(Long accountId, String receiverName, String accountCode, String description, String currency, String amount,String executionDate) throws Exception;
	
	FabrickResponse letturaTransazioni(Long accountId, String fromAccountingDate, String toAccountingDate) throws Exception;

}
