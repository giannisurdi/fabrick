package it.fabrick.testingweb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import it.fabrick.controller.FabrickController;


@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = true)
public class FabrickTest {
	
	@Autowired
	private FabrickController fabrickController;

	@Test
	public void contextLoads() {
		assertThat(fabrickController).isNotNull();
	}
	
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testSaldoOK() throws Exception {
		this.mockMvc.perform(get("/letturaSaldo?accountId=14537780")).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"status\":\"OK\"")));
	}
	
	@Test
	public void testSaldoKOBadRequest() throws Exception {
		this.mockMvc.perform(get("/letturaSaldo")).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("Parametri in ingresso obbligatori non popolati correttamente")));
	}
	
	@Test
	public void testSaldoKOForbidden() throws Exception {
		this.mockMvc.perform(get("/letturaSaldo?accountId=145")).andExpect(status().isForbidden());
	}
	
	@Test
	public void testBonificoKO() throws Exception {
		this.mockMvc.perform(post("/bonifico?accountId=14537780&amount=100&accountCode=IT23A0336844430152923804660&currency=EUR&description=CENA&executionDate=2021-01-01&receiverName=Giovan%20Battista%20Surdi"))
			.andExpect(status().isBadRequest()); 
	}
	
	@Test
	public void testBonificoKOBadequest() throws Exception {
		this.mockMvc.perform(post("/bonifico?accountId=14537780&amount=100&currency=EUR&description=CENA&executionDate=2021-01-01&receiverName=Giovan%20Battista%20Surdi"))
			.andExpect(status().isBadRequest())
			.andExpect(content().string(containsString("Parametri in ingresso obbligatori non popolati correttamente"))); 
	}
	
	@Test
	public void testLetturaTransazioniOK() throws Exception {
		this.mockMvc.perform(get("/letturaTransazioni?accountId=14537780&fromAccountingDate=2019-04-01&toAccountingDate=2021-04-01"))
			.andExpect(status().isOk())
				.andExpect(content().string(containsString("\"status\":\"OK\"")));
	}
	
	@Test
	public void testLetturaTransazioniKOBadRequest() throws Exception {
		this.mockMvc.perform(get("/letturaTransazioni?fromAccountingDate=2019-04-01&toAccountingDate=2021-04-01"))
		.andExpect(status().isBadRequest())
		.andExpect(content().string(containsString("Parametri in ingresso obbligatori non popolati correttamente")));
	}
	
	@Test
	public void testLetturaTransazioniKOForbiddend() throws Exception {
		this.mockMvc.perform(get("/letturaTransazioni?accountId=145&fromAccountingDate=2019-04-01&toAccountingDate=2021-04-01"))
		.andExpect(status().isForbidden());
	}

}
