-- Dump della struttura del database fabrick
CREATE DATABASE IF NOT EXISTS `fabrick` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fabrick`;

-- Dump della struttura di tabella fabrick.transazioni
CREATE TABLE IF NOT EXISTS `transazioni` (
  `transaction_id` bigint(20) NOT NULL,
  `accounting_date` varchar(10) DEFAULT NULL,
  `operation_id` varchar(100) DEFAULT NULL,
  `value_date` varchar(10) DEFAULT NULL,
  `enumeration_type` varchar(100) DEFAULT NULL,
  `value_type` varchar(100) DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;